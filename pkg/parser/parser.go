package parser

import (
	"strings"
	"errors"
	"strconv"
	"encoding/json"
	"fmt"
)

const (
	newline = "\n"

	extraSection  = "<------------Extra------------>"
	bodySection   = "<------------Body------------>"
	headerSection = "<------------Header------------>"

	empty = ""

	HeaderK = "Header"
	CookieK = "Cookie"
	QueryK  = "Query"
	FormK   = "Form"

	maxJSONDepth = 2
)

type Request struct {
	Params map[string]*Param

	Raw string

	IP          string
	StatusCode  uint16
	Referer     string
	UserAgent   string
	Host        string
	Time        string
	Method      string
	Dir         string
	ContentType string
}

type Param struct {
	Value    string
	Position string
}

func NewRequest(request string) (*Request, error) {
	request = strings.Replace(request, headerSection, empty, 1)
	if request == empty {
		return nil, errors.New("empty request")
	}
	r := &Request{
		Params: make(map[string]*Param),
		Raw:    request,
	}
	if err := r.parse(request); err != nil {
		return nil, err
	}
	return r, nil
}

func (r *Request) parse(request string) error {
	if extra := strings.Index(request, extraSection); extra == -1 {
		return errors.New("request don't have Extra")
	} else {
		if err := r.parseHeader(request[:extra]); err != nil {
			return err
		}

		body := strings.Index(request, bodySection)
		if body != -1 {
			if err := r.parseBody(request[body+len(bodySection):]); err != nil {
				return err
			}
		} else {
			body = len(request)
		}

		if err := r.parseExtra(request[extra+len(extraSection) : body]); err != nil {
			return err
		}
	}

	return nil
}

func (r *Request) parseHeader(header string) error {
	rows := strings.Split(header, newline)
	index := -1

	var query, cookie string

	for _, row := range rows {
		row = trim(row)
		if row == empty {
			continue
		}
		index++
		switch index {
		case 0:
			r.Time = row
			continue
		case 1:
			query = row
			continue
		}

		ex := strings.Index(row, ":")
		if ex == -1 {
			continue
		}
		key := row[:ex]
		value := row[ex+1:]
		switch strings.ToLower(key) {
		case "referer":
			r.Referer = trim(value)
			continue
		case "user-agent":
			r.UserAgent = trim(value)
		case "host":
			r.Host = trim(value)
			if ex := strings.Index(r.Host, ":"); ex != -1 {
				port := r.Host[ex+1:]
				if port == "80" {
					r.Host = r.Host[:ex]
				}
			}
			continue
		case "cookie":
			cookie = value
			continue
		case "content-type":
			r.ContentType = trim(value)
			if ex := strings.Index(r.ContentType, ";"); ex != -1 {
				r.ContentType = r.ContentType[:ex]
			}
		}

		r.addHeader(key, value, HeaderK)
	}

	if query != empty {
		r.parseQuery(query)
	}
	if cookie != empty {
		r.parseCookie(cookie)
	}

	return nil
}

func (r *Request) parseCookie(cookie string) error {
	params := strings.Split(cookie, ";")

	for _, param := range params {
		ex := strings.Index(param, "=")
		if ex == -1 {
			continue
		}
		r.addCookie(param[:ex], param[ex+1:], CookieK)
	}

	return nil
}

func (r *Request) parseQuery(query string) error {
	q := strings.Split(query, " ")
	if len(q) != 3 {
		return errors.New("query got error")
	}
	r.Method = q[0]
	r.Dir = q[1]

	ex := strings.Index(r.Dir, "?")
	if ex != -1 {
		queryValue := r.Dir[ex+1:]
		r.Dir = r.Dir[:ex]

		params := strings.Split(queryValue, "&")

		for _, param := range params {
			ex := strings.Index(param, "=")
			if ex == -1 {
				continue
			}
			r.addParam(param[:ex], param[ex+1:], QueryK)
		}
	}

	return nil
}

func (r *Request) parseBody(body string) error {
	if r.ContentType == "application/x-www-form-urlencoded" {
		params := strings.Split(body, "&")

		for _, param := range params {
			ex := strings.Index(param, "=")
			if ex == -1 {
				continue
			}
			r.addParam(param[:ex], param[ex+1:], FormK)
		}
	} else if r.ContentType == "application/json" {
		r.addParam("json_body", body, FormK)
	} else if r.ContentType == "application/xml" {
		// TODO: parse xml
	} else if r.ContentType == "multipart/form-data" {
		// TODO: parse multipart/form-data
	}

	return nil
}

func (r *Request) parseExtra(extra string) error {
	if strings.Contains(extra, extraSection) {
		return errors.New("request have many extras")
	}

	fields := strings.Split(extra, newline)

	for _, field := range fields {
		ex := strings.Index(field, ":")
		if ex == -1 {
			continue
		}
		key := field[:ex]
		value := trim(field[ex+1:])
		switch strings.ToLower(key) {
		case "ip":
			r.IP = value
		case "status code":
			code, err := strconv.Atoi(value)
			if err != nil {
				return err
			}
			r.StatusCode = uint16(code)
		case "content length":
			// not parse
		}
	}

	return nil
}

func (r *Request) addParam(key, value, position string) {
	k := fmt.Sprintf("%s|%s|%s", r.Host, r.Dir, trim(key))
	k = strings.ToLower(key)

	var m map[string]interface{}
	if err := json.Unmarshal([]byte(value), &m); err != nil {
		r.Params[k] = &Param{
			Value:    trim(value),
			Position: position,
		}
	} else {
		for k1, v1 := range m {
			r.parseMap(k1, v1, position, 0)
		}
	}
}

func (r *Request) addCookie(key, value, position string) {
	k := fmt.Sprintf("%s|%s", r.Host, trim(key))
	k = strings.ToLower(k)

	v := trim(value)
	r.Params[k] = &Param{
		Value:    v,
		Position: position,
	}
}

func (r *Request) addHeader(key, value, position string) {
	k := strings.ToLower(trim(key))
	v := trim(value)
	r.Params[k] = &Param{
		Value:    v,
		Position: position,
	}
}

func (r *Request) parseMap(key string, value interface{}, position string, depth int) {
	if depth >= maxJSONDepth {
		return
	}
	switch value.(type) {
	case string:
		r.addParam(key, value.(string), position)
	case map[string]interface{}:
		for k, v := range value.(map[string]interface{}) {
			r.parseMap(fmt.Sprintf("%s.%s", key, k), v, position, depth+1)
		}
	case float64:
		v := strconv.FormatFloat(value.(float64), 'f', 5, 32)
		r.addParam(key, v, position)
	case int:
		r.addParam(key, strconv.Itoa(value.(int)), position)
	}
}

func (r *Request) String() string {
	b, _ := json.Marshal(r)
	return string(b)
}

func trim(s string) string {
	return strings.TrimSpace(s)
}
