package parser

import (
	"testing"
	"io/ioutil"
	"strings"
	"fmt"
)

func TestNewRequest(t *testing.T) {
	b, err := ioutil.ReadFile("../../requests.log")
	if err != nil {
		panic(err)
	}

	requests := strings.Split(string(b), headerSection)
	for _, request := range requests {
		req, err := NewRequest(request + headerSection)
		if err != nil {
			fmt.Println(err)
			continue
		}
		fmt.Println(req)
	}
}
